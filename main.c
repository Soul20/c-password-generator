#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "time.h"
#include "math.h"

int str_getl (char * strarray){
	int l;
	for (l=0; strarray[l] != '\0'; l++);
	return l;
}

void genpasswd (char * password, char * characters, int length){
	for (int i=0; i < length; i++){
		password[i] = characters[ rand() % str_getl(characters) ];
	}
	password[length] = '\0'; // String needs null terminator
}

// Function to get value of command line argument
char * getcla (char * argv[], int argc, char * key){
	for (int i=1; i < argc; i++){
		if (strcmp (key, argv[i]) == 0 ){
			return argv[i+1];
		}
	}
	return NULL;
}

double entropy (char * password){
	int length = str_getl(password);

	// Find how many unique characters are in password
	char charset [length]; charset[0] = '\0';
	int currentIdx = 0;

	for (int i=0; i<length; i++){
		int newChar = 1;
		for (int ii=0; ii<length; ii++) if (password[i] == charset[ii]) newChar = 0;
		if (newChar) charset[currentIdx++] = password[i];
	}

	return log2(pow(str_getl(charset), length));
}

int main (int argc, char * argv[]) {
	srand(time(0));

	//Password Length
	char * argLength = getcla(argv, argc, "-l");
	int length = argLength != NULL ? atoi(argLength) : 16;

	char password [length];
	password[0] = '\0';

	// Create character set
	char * numbers		= "1234567890";
	char * characters	= "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
	char * specials		= "!\"$%^&*(){}[]:@~;'#<>?,./\\|`";

	char * argCharset = getcla(argv, argc, "-c");
	int bwCharset = argCharset != NULL ? atoi(argCharset) : 7;

	int USE_NUMS	= 1;
	int USE_CHARS	= 2;
	int USE_SPECIALS= 4;
	int useNums = 	(bwCharset & USE_NUMS) 		== USE_NUMS;
	int useChars = 	(bwCharset & USE_CHARS) 	== USE_CHARS;
	int useSpecs = 	(bwCharset & USE_SPECIALS) 	== USE_SPECIALS;

	int charsetSize =
		(useNums ? str_getl(numbers) : 0) +
		(useChars ? str_getl(characters) : 0) +
		(useSpecs ? str_getl(specials) : 0);

	char characterSet [charsetSize];
	characterSet[0] = '\0'; // strcat expect both arguments to be null terminated so this avoids problems

	if (useNums)	strcat(characterSet, numbers);
	if (useChars)	strcat(characterSet, characters);
	if (useSpecs)	strcat(characterSet, specials);

	genpasswd(password, characterSet, length);
	printf("%s\n", password);
	printf("%f\n", entropy(password));
	return 0;
}

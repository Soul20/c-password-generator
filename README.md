# C Password Generator

Password generator written in C

## Options
### -c
Bitwise value to rule which character can be used in the password 
1 = Numbers [0-9]
2 = Characters [a-Z]
4 = Special Characters
*defaults to 7*

### -l
Length of password
*default to 16*
